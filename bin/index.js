#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const axios = require('axios');
const Instance = require('../src/Instance');
const LogService = require('../src/services/LogService');
const logService = new LogService(console.log);

const yargsConfigurationOption = (yargs) => {
  yargs.option('config', {
    type: 'string',
    describe: 'The path to the Verdaccio configuration file.',
    default: './config.yml',
    requiresArg: true,
  });
};

const yargsLinkServiceCommand = (yargs) => {
  return yargs.command(
    'link-service',
    "Create a 'verdaccio' service symlink.",
    (yargs) => {
      yargsConfigurationOption(yargs);

      yargs.positional('before-script', {
        type: 'string',
        describe: 'An optional script to run before starting the instance.',
        requiresArg: true,
      });

      yargs.option('aws', {
        type: 'boolean',
        describe:
          'If given, the service will substitute environment variables from the EC2 instance user data into the configuration.',
        default: false,
        requiresArg: false,
      });
    },
    (argv) => {
      const serviceTemplate = fs.readFileSync(
        path.join(__dirname, '../verdaccio.service')
      );
      const serviceStartScriptPath = (() => {
        if (argv.aws) {
          return path.join(__dirname, '../service-start-aws.sh');
        }

        return path.join(__dirname, '../service-start.sh');
      })();
      const beforeScriptPath = (() => {
        if (!argv.beforeScript) {
          return path.join(__dirname, '../before-script-default.sh');
        }

        return argv.beforeScript;
      })();

      const service = serviceTemplate
        .toString()
        .replace('$SERVICE_START_SCRIPT', path.resolve(serviceStartScriptPath))
        .replace('$CONFIGURATION_PATH', path.resolve(argv.config))
        .replace('$BEFORE_SCRIPT_PATH', path.resolve(beforeScriptPath));

      fs.writeFileSync('/etc/systemd/system/verdaccio.service', service);
    }
  );
};

const yargsAwaitCommand = (yargs) => {
  return yargs.command(
    'await <url>',
    'Wait for a Verdaccio instance to become available and return good responses.',
    (yargs) => {
      yargs.positional('url', {
        type: 'string',
        describe: 'The base URL of the Verdaccio instance.',
        default: 'http://localhost:80/',
      });
    },
    async (argv) => {
      for (let count = 0; count < 600; count++) {
        try {
          const response = await axios.get(argv.url);

          if (response.data.indexOf('<title>Verdaccio</title>') >= 0) {
            return;
          }
        } catch (error) {
          console.log(error);
        }

        await new Promise((resolve, reject) => setTimeout(resolve, 10 * 1000));
      }

      return Promise.reject();
    }
  );
};

const yargsAddUserOptions = (yargs) => {
  yargs.option('allow-conflict', {
    type: 'boolean',
    describe:
      'If given, errors resulting from a user already existing will be ignored.',
    default: false,
    requiresArg: false,
  });

  yargs.option('force', {
    type: 'boolean',
    describe:
      'If given, the tool will temporarily alter the Verdaccio configuration to enable user registration.',
    default: false,
    requiresArg: false,
  });

  yargs.option('out', {
    type: 'string',
    describe: 'The path to output the tokens of any created users.',
    default: './tokens.json',
    requiresArg: true,
  });

  yargs.option('merge-out', {
    type: 'string',
    describe:
      'The path to output the tokens of any created users, but if it exists and is valid JSON the new tokens will be merged into it.',
    requiresArg: true,
  });
};

const writeTokensOutput = (argv, tokens) => {
  const tokensObject = Object.fromEntries(tokens);

  if (argv.out) {
    fs.writeFileSync(argv.out, JSON.stringify(tokensObject, null, '  '));
  }

  if (argv.mergeOut) {
    try {
      let currentTokens = {};

      try {
        currentTokens = JSON.parse(fs.readFileSync(argv.mergeOut).toString());
      } catch (error) {
        logService.warn(
          'Existing tokens file to merge with does not exist or is invalid.'
        );
      }

      const mergedTokens = {
        ...currentTokens,
        ...tokensObject,
      };

      fs.writeFileSync(argv.mergeOut, JSON.stringify(mergedTokens, null, '  '));
    } catch (error) {
      logService.warn('Could not merge out tokens.');
      logService.warn(error);
    }
  }
};

const yargsAddUserCommand = (yargs) => {
  return yargs.command(
    'add-user',
    'Register a single user.',
    (yargs) => {
      yargsConfigurationOption(yargs);

      yargs.option('username', {
        type: 'string',
        describe: 'The username of the user to create.',
        demandOption: true,
        requiresArg: true,
      });

      yargs.option('password', {
        type: 'string',
        describe: 'The password of the user to create.',
        demandOption: true,
        requiresArg: true,
      });

      yargs.option('email', {
        type: 'string',
        describe: 'The public e-mail address of the user to create.',
        demandOption: true,
        requiresArg: true,
      });

      yargsAddUserOptions(yargs);
    },
    async (argv) => {
      const { allowConflict, force } = argv;
      const configurationFilePath = argv.config;
      const tokenOutputFilePath = argv.out;
      const instance = new Instance(logService, configurationFilePath);
      const addUser = async () => {
        await instance.start();

        const tokens = new Map();

        try {
          const { username, password, email } = argv;
          const response = await instance.addUser(username, password, email);
          const { token } = response.data;

          tokens.set(username, token);

          logService.info(
            `Added a new user '${username}' with token <${token}>`
          );
        } catch (error) {
          if (error.response.status === 409 && allowConflict) {
            logService.warn("The user already exists, but that's okay");
          } else {
            logService.error('Failed to add user:');
            logService.error(error);
          }
        }

        writeTokensOutput(argv, tokens);

        await instance.stop();
      };

      if (force) {
        const originalMaxUsers = instance.getConfigurationMaxUsers();
        instance.setConfigurationMaxUsers('+infinity');

        await instance.writeConfiguration();

        try {
          await addUser();
        } catch (error) {}

        instance.setConfigurationMaxUsers(originalMaxUsers);

        await instance.writeConfiguration();
      } else {
        await addUser();
      }
    }
  );
};

const yargsAddUsersCommand = (yargs) => {
  return yargs.command(
    'add-users',
    'Register multiple users.',
    (yargs) => {
      yargsConfigurationOption(yargs);

      yargs.option('file', {
        type: 'string',
        describe: 'The file containing data specifying the users to create.',
        demandOption: true,
        requiresArg: true,
      });

      yargsAddUserOptions(yargs);
    },
    async (argv) => {
      const { allowConflict, force, file } = argv;
      const tokenOutputFilePath = argv.out;

      let usersData;

      try {
        usersData = JSON.parse(fs.readFileSync(file));
      } catch (error) {
        logService.error(`Could not read or parse file '${file}'`);

        return;
      }

      const configurationFilePath = argv.config;
      const instance = new Instance(logService, configurationFilePath);
      const addUsers = async () => {
        await instance.start();

        const tokens = new Map();

        await Promise.all(
          usersData.map(async (userData) => {
            try {
              const { username, password, email } = userData;
              const response = await instance.addUser(
                username,
                password,
                email
              );
              const { token } = response.data;

              tokens.set(username, token);

              logService.info(
                `Added a new user '${username}' with token <${token}>`
              );
            } catch (error) {
              if (error.response.status === 409 && allowConflict) {
                logService.warn(
                  `The user '${userData.username}' already exists, but that's okay`
                );
              } else {
                logService.error(`Failed to add user '${userData.username}':`);
                logService.error(error);
              }
            }
          })
        );

        writeTokensOutput(argv, tokens);

        await instance.stop();
      };

      if (force) {
        const originalMaxUsers = instance.getConfigurationMaxUsers();
        instance.setConfigurationMaxUsers('+infinity');

        await instance.writeConfiguration();

        try {
          await addUsers();
        } catch (error) {}

        instance.setConfigurationMaxUsers(originalMaxUsers);

        await instance.writeConfiguration();
      } else {
        await addUsers();
      }
    }
  );
};

const yargsRemoveAwsCredentialsCommand = (yargs) => {
  return yargs.command(
    'remove-aws-credentials',
    'Remove the AWS credentials from the configuration file (for use with verdaccio-aws-s3-storage plugin).',
    (yargs) => {
      yargsConfigurationOption(yargs);
    },
    async (argv) => {
      const configurationFilePath = argv.config || null;
      const instance = new Instance(logService, configurationFilePath);
      instance.removeConfigurationAwsCredentials();

      await instance.writeConfiguration();
    }
  );
};

const yargs = require('yargs')
  .scriptName('verdaccio-utilities')
  .usage('$0 <command> [arguments]');

yargsLinkServiceCommand(yargs);
yargsAwaitCommand(yargs);
yargsAddUserCommand(yargs);
yargsAddUsersCommand(yargs);
yargsRemoveAwsCredentialsCommand(yargs);

yargs.help().argv;
