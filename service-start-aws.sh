#!/bin/bash
mkdir -p /etc/verdaccio
substitute-with "$1" --aws > /etc/verdaccio/config.yml
sh "$2" /etc/verdaccio/config.yml
verdaccio --config /etc/verdaccio/config.yml