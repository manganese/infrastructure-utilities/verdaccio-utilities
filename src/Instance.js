const PROCESS_START_ERROR_WATCH_DURATION = 1000;
const PROCESS_START_TIMEOUT_DURATION = 10000;

const { spawn } = require('child_process');
const fs = require('fs');
const url = require('url');

const yaml = require('yaml');
const axios = require('axios');

const get = require('lodash/get');
const set = require('lodash/set');
const unset = require('lodash/unset');
const setOrDelete = (object, key, value) => {
  if (!value) {
    unset(object, key);
  } else {
    set(object, key, value);
  }
};

class Instance {
  constructor(logService, configurationFilePath) {
    this.logService = logService;
    this.configurationFilePath = configurationFilePath;

    const configurationFileContents = fs
      .readFileSync(this.configurationFilePath)
      .toString();

    this.configuration = yaml.parse(configurationFileContents);
    this.api = axios.create({
      baseURL: `http://localhost:${this.port}/`,
    });
  }

  // Process
  start() {
    if (this.startPromise) {
      return this.startPromise;
    } else if (this.started) {
      this.logService.warn('The instance is already started, aborting.');

      return;
    }

    const promise = (this.startPromise = new Promise((resolve, reject) => {
      const instance = this;
      const process = (this.process = spawn('verdaccio', [
        '--config',
        this.configurationFilePath,
      ]));

      let didOutputHttp = false;
      let errorTimer = null;

      const handleData = (lineBuffer) => {
        const line = lineBuffer.toString();

        if (!didOutputHttp && line.indexOf('--- http address - ') >= 0) {
          didOutputHttp = true;

          errorTimer = setTimeout(() => {
            processStarted();
          }, PROCESS_START_ERROR_WATCH_DURATION);
        }

        if (
          line.indexOf(' fatal---') >= 0 ||
          (line.indexOf(' error---') >= 0 &&
            line.indexOf('s3: [_getData] err: no such package') < 0 &&
            line.indexOf('s3: [_getData] err 404 create new') < 0)
        ) {
          processNotStarted(line);
        }

        this.logService.debug(
          'Instance stdout: ' + line.replace(/[\n\r]$/, '')
        );
      };

      process.stdout.on('data', handleData);

      const timeoutTimer = setTimeout(
        processNotStarted.bind(null, 'Timed out'),
        PROCESS_START_TIMEOUT_DURATION
      );

      const clearPromise = () => {
        this.startPromise = null;
      };
      const clearTimers = () => {
        clearTimeout(timeoutTimer);

        try {
          clearTimeout(errorTimer);
        } catch (error) {}
      };
      const clearEvents = () => {
        process.stdout.removeListener('data', handleData);
      };
      const clearProcess = () => {
        process.kill('SIGKILL');
      };
      const clear = () => {
        clearPromise();
        clearTimers();
        clearEvents();
      };

      function processStarted() {
        clear();

        instance.started = true;

        resolve();
      }

      function processNotStarted(error) {
        instance.logService.error('Could not start the instance:');
        instance.logService.error(error);

        clear();
        clearProcess();
        reject();
      }
    }));

    return promise;
  }

  stop() {
    if (!this.started) {
      this.logService.warn('The instance is already stopped, aborting.');

      return Promise.resolve();
    }

    this.process.kill();

    return Promise.resolve();
  }

  // Configuration
  writeConfiguration() {
    fs.writeFileSync(
      this.configurationFilePath,
      yaml.stringify(this.configuration)
    );
  }

  getConfigurationMaxUsers() {
    return get(this.configuration, 'auth.htpasswd.max_users');
  }

  setConfigurationMaxUsers(value) {
    return set(this.configuration, 'auth.htpasswd.max_users', value);
  }

  getConfigurationStorage() {
    return get(this.configuration, 'storage');
  }

  setConfigurationStorage(value) {
    return setOrDelete(this.configuration, 'storage', value);
  }

  getConfigurationStore() {
    return get(this.configuration, 'store');
  }

  setConfigurationStore(value) {
    return setOrDelete(this.configuration, 'store', value);
  }

  removeConfigurationAwsCredentials() {
    const store = this.getConfigurationStore();

    if (!store) return;

    const awsS3Storage = store['aws-s3-storage'];

    if (!awsS3Storage) return;

    ['accessKeyId', 'secretAccessKey', 'sessionToken'].forEach((key) => {
      unset(awsS3Storage, key);
    });
  }

  // API
  get urlBase() {
    let { listen } = this.configuration;

    if (!listen) {
      listen = '0.0.0.0:4873';
    }

    if (listen.startsWith('http')) {
      return listen;
    }

    return 'http://' + listen;
  }

  get port() {
    const listenUrl = url.parse(this.urlBase);

    return listenUrl.port;
  }

  addUser(username, password, email) {
    return this.api.put(`/-/user/org.couchdb.user:${username}`, {
      type: 'user',
      _id: 'org.couchdb.user:' + username,
      name: username,
      password,
      email,
      date: new Date().toISOString(),
    });
  }
}

module.exports = Instance;
