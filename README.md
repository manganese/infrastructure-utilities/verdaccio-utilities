# Manganese - Infrastructure Utilities - Verdaccio Utilities

A small utility for working with [Verdaccio](https://verdaccio.org) hosts. The main use case is adding users while provisioning an AMI and subsequently disabling user registration in the configuration.

## Installation

```bash
npm install -g verdaccio-utilities
```

## License

This package is licensed under the MIT license to match Verdaccio.
