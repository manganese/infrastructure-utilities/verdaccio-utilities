#!/bin/bash
mkdir -p /etc/verdaccio
cp -u "$1" /etc/verdaccio/config.yml
sh "$2" /etc/verdaccio/config.yml
verdaccio --config /etc/verdaccio/config.yml